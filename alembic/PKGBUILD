# Maintainer: N30N <archlinux@alunamation.com>

pkgname="alembic"
pkgver=1.5.5
pkgrel=5
pkgdesc="An open framework for storing and sharing 3D geometry data"
url="http://alembic.io"
license=("MIT")
arch=("i686" "x86_64")
depends=("hdf5" "python2-ilmbase")
makedepends=("cmake" "boost" "openexr" "glut")
optdepends=("glut: for SimpleAbcViewer" "glu: for SimpleAbcViewer")
source=("http://alembic.googlecode.com/archive/${pkgver//./_0}.tar.gz")
sha1sums=('49a0304a13e36859c284558a704341589526acac')
OPTIONS=('staticlibs')

prepare() {
	[ -d build ] && rm -rf build
	mkdir build

	cd "${pkgname}-${pkgver//./_0}"
	sed "s/BOOST_PYTHON_LIBRARY/Boost_PYTHON_LIBRARY/g" \
		-i python/examples/AbcView/CMakeLists.txt
}

build() {
	cd build

	cmake -DLIBPYTHON_VERSION=2.7 -DWITH_PYALEMBIC=1  -DALEMBIC_NO_TESTS=1 \
		-DBoost_PYTHON_LIBRARY_RELEASE="/usr/lib/libboost_python.so" \
		-DCMAKE_INSTALL_PREFIX="${pkgdir}/usr" \
		"../${pkgname}-${pkgver//./_0}"
	make
}


check() {
	cd build
	make test
}

package() {
	install -Dm644 "${pkgname}-${pkgver//./_0}/LICENSE.txt" \
		"${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

	cd build
	make install

	install -d "${pkgdir}/usr/lib"
	mv ${pkgdir}/usr/${pkgname}-${pkgver}/lib/static/* ${pkgdir}/usr/lib
	mv ${pkgdir}/usr/${pkgname}-${pkgver}/lib/* ${pkgdir}/usr/lib
	mv ${pkgdir}/usr/${pkgname}-${pkgver}/include ${pkgdir}/usr
	mv ${pkgdir}/usr/${pkgname}-${pkgver}/bin ${pkgdir}/usr

	install -d "${pkgdir}/usr/lib/python2.7/site-packages"
	mv "${pkgdir}/usr/lib/alembicmodule.so" "${pkgdir}/usr/lib/python2.7/site-packages"
	mv "${pkgdir}/usr/lib/alembicglmodule.so" "${pkgdir}/usr/lib/python2.7/site-packages"

	rm -r "${pkgdir}/usr/${pkgname}-${pkgver}"
	rm -r "${pkgdir}/usr/lib/static/"
}

# vim: set noet ff=unix
